package tech.quantit.northstar.common.constant;

/**
 * 回测精度
 * @author KevinHuangwl
 *
 */
public enum PlaybackPrecision {
	TICK,
	BAR
}
