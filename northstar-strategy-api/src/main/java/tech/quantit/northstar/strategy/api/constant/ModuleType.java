package tech.quantit.northstar.strategy.api.constant;

public enum ModuleType {
	/**
	 * 趋势类
	 */
	CTA, 
	/**
	 * 套利类
	 */
	ARBITRAGE;
}
